// var elementos = [];

// elementos.push("Emmanual");
// elementos.push("")

// var familia = [];
// var persona = [];
// persona.push("Emmanuel");
// persona.push("Torres");
// persona.push("25");

// var gustos = [];
// gustos.push("Musica");
// gustos.push("Programar");

// persona.push(gustos);

// familia.push(persona);

// console.log(familia);

//Minimo 4 familiares con 5 datos individuales
//Gustos unos 5

var familia = 
{
    "Persona 1": 
    {
        "Nombre" : "Marco Antonio",
        "Apellidos" : "Sanchez Barcenas",
        "gustos":
        {
            "Musica" : ["Los angeles azules", "Juan Gabriel"],
            "Dulces" : ["Cholate", "Frituras", "Masapan"],
        },
    },

    "Persona 2": 
    {
        "Nombre" : "Marco Antonio",
        "Apellidos" : "Sanchez Fermin",
        "gustos":
        {
            "Musica" : "Con ritmo",
            "Dulces" : "Chocolate",
        },
    },

    "Persona 3": 
    {
        "Nombre" : "Perla Yunuet",
        "Apellidos" : "Sanchez Fermin",
        "gustos":
        {
            "Musica" : ["BTS", "BlackPink"],
            "Dulces" : ["Chocolate Blanco", "Cheetos"],
            "Frutas" : ["Fresa", "Mango", "Uvas"],
        },
    },

    "Persona 4": 
    {
        "Nombre" : "Filiberta",
        "Apellidos" : "Fermin Bernandino",
        "gustos":
        {
            "Musica" : ["Los Angeles Azules", "Paquita la del barrio"],
            "Dulces" : ["Chocolate", "Galletas", "Jugo", "Barritas"],
            "Frutas" : ["Fresa", "Mango", "Uvas", "Coco"],
        },
    },

    "Persona 5": 
    {
        "Nombre" : "Ruby",
        "Apellidos" : "Sanchez Fermin",
        "gustos":
        {
            "Musica" : ["Los Angeles Azules", "Juan Grabriel"],
            "Dulces" : ["Galletas", "Jugo", "Barritas"],
            "Frutas" : ["Fresa", "Mango", "Uvas", "Coco"], 
        },
    },
}
console.log(familia);